# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { 'zaq@zaq.com' }
    password { 'zaq1@WSX' }
    first_name { 'Roman' }
    last_name { 'Kowalski' }
  end
end
