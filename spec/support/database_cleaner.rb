# frozen_string_literal: true

RSpec.configure do |config|
  config.use_transactional_fixtures = false

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before do
    DatabaseCleaner.strategy = Capybara.current_driver == :rack_test ? :transaction : :truncation

    DatabaseCleaner.start
  end
  config.append_after do
    DatabaseCleaner.clean
  end
end
