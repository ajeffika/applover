# frozen_string_literal: true

# This file is copied to spec/ when you run 'rails generate rspec:install'
#
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../config/environment', __dir__)
# Prevent database truncation if the environment is production
abort('The Rails environment is running in production mode!') if Rails.env.production? || Rails.env.staging?
require 'rspec/rails'
require 'factory_bot'
require 'database_cleaner'

Dir[Rails.root.join('spec/support/**/*.rb')].sort.each { |f| require f }

RSpec.configure do |config|
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!

  config.include Capybara::DSL
  config.include FactoryBot::Syntax::Methods
  config.include ActionView::Helpers::NumberHelper
  config.include ActionView::Helpers::SanitizeHelper

  config.before :suite do
    ActiveRecord::Migration.check_pending! if defined?(ActiveRecord::Migration)
  end

  Shoulda::Matchers.configure do |conf|
    conf.integrate do |with|
      with.test_framework :rspec
      with.library :rails
    end
  end
end
