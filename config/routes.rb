# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'cities#index'
  resources :cities
  resources :users, only: %i[show edit update destroy]

  ### Authorization routes
  get 'sign_up', to: 'authorization/registrations#new'
  post 'sign_up', to: 'authorization/registrations#create'
  get 'sign_in', to: 'authorization/sessions#new'
  post 'sign_in', to: 'authorization/sessions#create', as: 'log_in'
  delete 'logout', to: 'authorization/sessions#destroy'
  get 'password', to: 'authorization/passwords#edit', as: 'edit_password'
  patch 'password', to: 'authorization/passwords#update'
  get 'password/reset', to: 'authorization/password_resets#new'
  post 'password/reset', to: 'authorization/password_resets#create'
  get 'password/reset/edit', to: 'authorization/password_resets#edit'
  patch 'password/reset/edit', to: 'authorization/password_resets#update'
  ###
end
