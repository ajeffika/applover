# frozen_string_literal: true

module CoreExtensions
  module Hash
    def to_o
      JSON.parse to_json, object_class: OpenStruct
    end
  end
end

Hash.include CoreExtensions::Hash
