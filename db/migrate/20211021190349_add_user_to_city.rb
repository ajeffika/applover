# frozen_string_literal: true

class AddUserToCity < ActiveRecord::Migration[6.1]
  def change
    add_reference :cities, :user
  end
end
