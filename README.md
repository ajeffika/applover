In order to run the app, just run

```rails db:create db:migrate db:seed```

and then

``` rails s ```

You can create an account and then add countries to your account.

Only user which added the city can see it.

When a city which is not available in OpenWeather an information is shown informing that such city has not been found

OpenWeather shares list of all cities, which we could use for more sophisticated implementation. We could store all those cities
in our database and let user add these to favourites. In that case we would get around all missspelling errors
and be able to always provide weather data for a city

Right now the solution is not perfect, and can be a lot better in terms of reliablity

User can edit his name and email via profile tab.

