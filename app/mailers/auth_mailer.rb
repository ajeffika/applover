# frozen_string_literal: true

class AuthMailer < ApplicationMailer
  def account_created(user)
    @user = user
    @url = "#{Settings.base_url}/sign_in"
    mail(to: @user.email, subject: 'Welcome to weather app!')
  end

  def reset_password
    @token = params[:user].signed_id(purpose: 'password_reset', expires_in: 15.minutes)
    mail to: params[:user].email
  end
end
