# frozen_string_literal: true

class UsersController < BaseController
  def edit; end

  def update
    current_user.assign_attributes(user_params)
    if current_user.save
      redirect_to user_path(current_user)
    else
      render :edit
    end
  end

  def destroy
    session[:user_id] = nil
    user = User.find(params[:id])
    user.destroy
    redirect_to sign_in_path
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email)
  end
end
