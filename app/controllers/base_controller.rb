# frozen_string_literal: true

class BaseController < ApplicationController
  before_action :require_user_logged_in!
end
