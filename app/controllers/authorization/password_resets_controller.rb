# frozen_string_literal: true

module Authorization
  class PasswordResetsController < ApplicationController
    def new; end

    def edit
      @user = User.find_signed!(password_params[:token], purpose: 'password_reset')
    rescue ActiveSupport::MessageVerifier::InvalidSignature
      redirect_to sign_in_path, alert: 'Your token has expired. Please try again.'
    end

    def create
      @user = User.find_by(email: password_params[:email])
      AuthMailer.with(user: @user).reset_password.deliver_now if @user.present?
      redirect_to root_path, notice: 'Please check your email to reset the password'
    end

    def update
      @user = User.find_signed!(password_params[:token], purpose: 'password_reset')
      if @user.update(password_params)
        redirect_to sign_in_path, notice: 'Your password was reset successfully. Please sign in'
      else
        render :edit
      end
    end

    private

    def password_params
      params.require(:user).permit(:email, :token, :password, :password_confirmation)
    end
  end
end
