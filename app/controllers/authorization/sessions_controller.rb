# frozen_string_literal: true

module Authorization
  class SessionsController < ApplicationController
    skip_before_action :verify_authenticity_token, only: :create

    def new; end

    def create
      user = User.find_by(email: user_params[:email])
      if user.present? && user.authenticate(user_params[:password])
        session[:user_id] = user.id
        redirect_to root_path, notice: 'Logged in successfully'
      else
        flash.now[:alert] = 'Invalid email or password'
        render :new
      end
    end

    def destroy
      session[:user_id] = nil
      redirect_to root_path, notice: 'Logged Out'
    end

    private

    def user_params
      params.require(:user).permit(:password, :email)
    end
  end
end
