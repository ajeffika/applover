# frozen_string_literal: true

class ApplicationController < ActionController::Base
  helper_method :current_user, :logged_in?

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    !!current_user
  end

  def require_user_logged_in!
    redirect_to sign_in_path, alert: 'You must be signed in' if current_user.nil?
  end
end
