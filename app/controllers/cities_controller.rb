# frozen_string_literal: true

class CitiesController < BaseController
  before_action :find_city, only: %i[show edit update destroy]
  before_action :build_city, only: %i[new create]
  def index
    @cities = current_user.cities
  end

  def new; end

  def edit; end

  def show
    @weather = Weather::FetchWeather.new(@city).call
    @weather_body = @weather.body
  end

  def create
    @city.assign_attributes(city_params)
    if @city.save
      redirect_to city_path(@city)
    else
      render :edit
    end
  end

  def update
    @city.assign_attributes(city_params)
    if @city.save
      redirect_to city_path(@city)
    else
      render :edit
    end
  end

  def destroy
    @city.destroy
    redirect_to cities_path
  end

  private

  def city_params
    params.require(:city).permit(:country, :name)
  end

  def build_city
    @city = current_user.cities.new
  end

  def find_city
    @city = current_user.cities.find(params[:id])
  end
end
