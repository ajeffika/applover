# frozen_string_literal: true

module Weather
  class FetchWeather
    attr_accessor :city, :country

    def initialize(city)
      @country = city.country
      @city = city.name
    end

    def call
      fetch_weather
    end

    def fetch_weather
      response = Faraday.get("#{base_url}#{params}")
      response.body
      { body: JSON.parse(response.body), fetched: response.status == 200 }.to_o
    end

    def base_url
      'https://api.openweathermap.org/data/2.5/weather'
    end

    def params
      "?q=#{city},#{country}&units=metric&appid=#{Settings.weather_api_key}"
    end
  end
end
